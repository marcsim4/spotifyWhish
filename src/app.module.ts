import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AlbumModule } from './Album/album.module';
import { ArtistModule } from './Artist/artist.module';
import { SongModule } from './Song/song.module';
import { UserModule } from './User/user.module';
import { typeormConfig } from './config/typeorm.config';
import { PlaylistModule } from './playlist/playlist.module';
@Module({
  imports: [
    AlbumModule,
    ArtistModule,
    SongModule,
    UserModule,
    PlaylistModule,
    TypeOrmModule.forRoot(typeormConfig),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
