import { Album } from 'src/Album/models/album.entity';
import {
  BaseEntity,
  Column,
  Entity,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Artist extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  isBand: boolean;
  @ManyToMany(() => Album, (album) => album.artists)
  albums: Album[];
}
