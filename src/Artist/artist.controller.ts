import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
} from '@nestjs/common';
import { ArtistService } from './artist.service';
import { AddArtistDTO } from './dto/add-artist.dto';
import { UpdateArtistDTO } from './dto/update-artist.dto';
import { Artist } from './models/artist.entity';

@Controller('artist')
export class ArtistController {
  constructor(private artistService: ArtistService) {}

  @Post('/addArtist')
  async createArtist(@Body() AddArtistDTO: AddArtistDTO) {
    return this.artistService.createArtist(AddArtistDTO);
  }

  @Get('/:id')
  async getArtistById(@Param('id', ParseIntPipe) id: number): Promise<Artist> {
    return this.artistService.getArtistById(id);
  }

  @Get()
  async getAllArtists() {
    return this.artistService.getAllArtists();
  }

  @Delete(':id')
  async deleteArtistById(@Param() params) {
    return this.artistService.deleteArtistById(params.id);
  }

  @Patch('/:id')
  //@UsePipes(Validator)
  updateStatusById(
    @Param('id', ParseIntPipe) id: number,
    @Body()
    UpdateArtistDTO: UpdateArtistDTO,
  ) {
    return this.artistService.updateArtist(UpdateArtistDTO);
  }
}
