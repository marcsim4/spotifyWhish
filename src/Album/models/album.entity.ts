import { Artist } from 'src/Artist/models/artist.entity';
import { Song } from 'src/Song/models/song.entity';
import {
  BaseEntity,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Album extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  title: string;
  @Column()
  year: string; //Todo date format
  @Column()
  cover: string;
  @ManyToMany(() => Artist, (artist) => artist.albums)
  @JoinTable()
  artists: Artist[];
  @OneToMany(() => Song, (song) => song.albums)
  songs: Song[];
}
