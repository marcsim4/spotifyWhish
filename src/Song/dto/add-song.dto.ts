import { IsNotEmpty, IsNumber } from 'class-validator';
import { Album } from 'src/Album/models/album.entity';
export class AddSongDTO {
  @IsNotEmpty({ message: 'titre est vide.' })
  title: string;
  @IsNumber()
  duration: number;
  album?: Album;
}
