import { Album } from 'src/Album/models/album.entity';
import { Playlist } from 'src/playlist/models/playlist.entity';
import {
  BaseEntity,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Song extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  title: string;
  @Column()
  duration: number;
  @ManyToOne(() => Album, (album) => album.songs, { onDelete: 'SET NULL' })
  albums: Album;
  @ManyToMany(() => Playlist)
  @JoinTable()
  playlists: Playlist[];
}
