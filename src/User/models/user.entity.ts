import { Playlist } from 'src/playlist/models/playlist.entity';
import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';

@Entity()
@Unique(['mail'])
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  mail: string;
  @Column()
  password: string;
  @OneToMany(() => Playlist, (playlists) => playlists.user)
  playlists: Playlist[];
}
