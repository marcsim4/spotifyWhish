import { IsNotEmpty } from 'class-validator';
import { Song } from 'src/Song/models/song.entity';
import { User } from 'src/User/models/user.entity';

export class AddPlaylistDTO {
  @IsNotEmpty({ message: 'le nom est vide.' })
  name: string;
  user?: User;
  songs?: Song[];
}
