import { IsNotEmpty } from 'class-validator';
import { Song } from 'src/Song/models/song.entity';

export class UpdatePlaylistDTO {
  id: number;
  @IsNotEmpty({ message: 'le nom est vide.' })
  name: string;
  songs: Song[];
}
