import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AddPlaylistDTO } from './dto/add-playlist.dto';
import { UpdatePlaylistDTO } from './dto/update-playlist.dto';
import { Playlist } from './models/playlist.entity';
import { PlaylistService } from './playlist.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('playlist')
export class PlaylistController {
  constructor(private playlistService: PlaylistService) {}

  @Post('/addPlaylist')
  async createPlaylist(@Body() AddPlaylistDTO: AddPlaylistDTO) {
    return this.playlistService.createPlaylist(AddPlaylistDTO);
  }

  @Get('/:id')
  async getPlaylistById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<Playlist> {
    return this.playlistService.getPlaylistById(id);
  }

  @Get()
  async getAllPlaylists() {
    return this.playlistService.getAllPlaylist();
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  async deletePlaylistById(@Param() params) {
    return this.playlistService.deletePlaylistById(params.id);
  }

  @Patch('/:id')
  //@UsePipes(Validator)
  updateStatusById(
    @Param('id', ParseIntPipe) id: number,
    @Body()
    UpdatePlaylistDTO: UpdatePlaylistDTO,
  ) {
    return this.playlistService.updatePlaylist(UpdatePlaylistDTO);
  }
}
