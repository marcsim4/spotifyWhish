import { EntityRepository, Repository } from 'typeorm';
import { AddPlaylistDTO } from '../dto/add-playlist.dto';
import { UpdatePlaylistDTO } from '../dto/update-playlist.dto';
import { Playlist } from './playlist.entity';

@EntityRepository(Playlist)
export class PlaylistRepository extends Repository<Playlist> {
  async createPlaylist(addPlaylistDTO: AddPlaylistDTO): Promise<Playlist> {
    const { name, songs } = addPlaylistDTO;
    const playlist = new Playlist();
    playlist.name = name;
    if (songs) {
      playlist.songs = songs;
    }
    return await playlist.save();
  }

  async updatePlaylist(
    playlist: Playlist,
    updatePlaylistDTO: UpdatePlaylistDTO,
  ): Promise<Playlist> {
    playlist.name = updatePlaylistDTO.name;
    playlist.songs = updatePlaylistDTO.songs;
    return await playlist.save();
  }

  async deletePlaylist(playlist: Playlist): Promise<Playlist> {
    return await playlist.remove();
  }
}
