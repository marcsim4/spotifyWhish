import { Song } from 'src/Song/models/song.entity';
import { User } from 'src/User/models/user.entity';
import {
  BaseEntity,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Playlist extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @ManyToOne(() => User, (user) => user.playlists)
  user: User;
  @ManyToMany(() => Song)
  @JoinTable()
  songs: Song[];
}
