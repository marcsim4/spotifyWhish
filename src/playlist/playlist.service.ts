import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AddPlaylistDTO } from './dto/add-playlist.dto';
import { UpdatePlaylistDTO } from './dto/update-playlist.dto';
import { Playlist } from './models/playlist.entity';
import { PlaylistRepository } from './models/playlist.repository';

@Injectable()
export class PlaylistService {
  constructor(
    @InjectRepository(PlaylistRepository)
    private playlistRepository: PlaylistRepository,
  ) {}

  async createPlaylist(addPlaylistDTO: AddPlaylistDTO) {
    return await this.playlistRepository.createPlaylist(addPlaylistDTO);
  }

  async getPlaylistById(id: number): Promise<Playlist> {
    const found = await this.playlistRepository.findOne(id, {
      relations: ['songs'],
    });
    if (!found) {
      throw new NotFoundException(`Playlist with ID "${id}" not found`);
    }
    return found;
  }

  async getPlaylistByName(name: string): Promise<Playlist> {
    const found = await this.playlistRepository.findOne({ name: name });
    if (!found) {
      throw new NotFoundException(`Playlist with name "${name}" not found`);
    }
    return found;
  }

  async getAllPlaylist(): Promise<Playlist[]> {
    const found = await this.playlistRepository.find();
    if (!found) {
      throw new NotFoundException(`Playlist not found`);
    }
    return found;
  }

  async updatePlaylist(UpdatePlaylistDTO: UpdatePlaylistDTO) {
    const found = this.getPlaylistById(UpdatePlaylistDTO.id);
    return await this.playlistRepository.updatePlaylist(
      await found,
      UpdatePlaylistDTO,
    );
  }

  async deletePlaylistById(id: number): Promise<Playlist> {
    const found = await this.playlistRepository.findOne(id);
    if (!found) {
      throw new NotFoundException(`Playlist with ID "${id}" not found`);
    } else {
      await this.playlistRepository.deletePlaylist(found);
      return found;
    }
    //await this.artistRepository.deleteArtist(await this.getArtistById(id));
  }
}
